<?php

namespace SharedKernel;

/**
 * Class Email
 * @package SharedKernel
 */
class Email
{
    /**
     * @var string
     */
    private $email;

    /**
     * Email constructor.
     * @param $email
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
<?php

namespace Aquarium\Provider;

use Aquarium\Provider;
use SharedKernel\Email;

/**
 * Interface MailProvider
 * @package Aquarium\Provider
 */
interface MailProvider extends Provider
{
    /**
     * @param Email $email
     */
    public function addRecipient(Email $email);
}

<?php

namespace Aquarium\Provider\MailProvider;

use Aquarium\Provider\MailProvider;
use SharedKernel\Email;

/**
 * Class GetResponseMailProvider
 * @package Aquarium\Provider\MailProvider
 */
class GetResponseMailProvider implements MailProvider
{
    /**
     * @var Email
     */
    private $recipient;

    /**
     * @param Email $email
     */
    public function addRecipient(Email $email)
    {
        $this->recipient = $email;
    }

    public function send()
    {
        if (is_null($this->recipient)) {
            throw new \Exception('Recipient can not be null.');
        }

        // implement getresponse api
    }
}
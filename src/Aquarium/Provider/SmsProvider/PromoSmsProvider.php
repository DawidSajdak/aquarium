<?php

namespace Aquarium\Provider\SmsProvider;

use Aquarium\Provider\SmsProvider;
use SharedKernel\MobilePhone;

/**
 * Class PromoSmsProvider
 * @package Aquarium\Provider\SmsProvider
 */
class PromoSmsProvider implements SmsProvider
{
    /**
     * @var MobilePhone
     */
    private $recipient;

    public function send()
    {
        if (is_null($this->recipient)) {
            throw new \Exception('Recipient can not be null.');
        }

        // implement promo sms api
    }

    /**
     * @param MobilePhone $mobilePhone
     */
    public function addRecipient(MobilePhone $mobilePhone)
    {
        $this->recipient = $mobilePhone;
    }
}
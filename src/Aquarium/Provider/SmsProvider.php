<?php

namespace Aquarium\Provider;

use Aquarium\Provider;
use SharedKernel\MobilePhone;

/**
 * Interface SmsProvider
 * @package Aquarium\Provider
 */
interface SmsProvider extends Provider
{
    /**
     * @param MobilePhone $mobilePhone
     */
    public function addRecipient(MobilePhone $mobilePhone);
}
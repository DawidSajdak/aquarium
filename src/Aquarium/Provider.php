<?php

namespace Aquarium;

/**
 * Interface Provider
 * @package Aquarium
 */
interface Provider
{
    public function send();
}
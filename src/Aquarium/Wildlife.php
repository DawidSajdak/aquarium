<?php

namespace Aquarium;

/**
 * Interface Wildlife
 * @package Aquarium
 */
interface Wildlife
{
    public function breath();
}
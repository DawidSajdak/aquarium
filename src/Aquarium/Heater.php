<?php

namespace Aquarium;

/**
 * Class Heater
 * @package Aquarium
 */
class Heater
{
    const COLD = 1;
    const MEDIUM = 2;
    const WARM = 3;

    /**
     * @var int
     */
    private $warmLevel;

    /**
     * Heater constructor.
     * @param $warmLevel
     */
    public function __construct($warmLevel)
    {
        $this->warmLevel = $warmLevel;
    }

    public function warmUp()
    {
        if ($this->warmLevel === self::WARM) {
            throw new \Exception('Horry sheet, it is too warm, calm down dude!');
        }
        $this->warmLevel = $this->warmLevel++;
    }

    public function cooldown()
    {
        if ($this->warmLevel === self::COLD) {
            throw new \Exception('Horry sheet, it is too cold, make some fire!');
        }
        $this->warmLevel = $this->warmLevel--;
    }
}
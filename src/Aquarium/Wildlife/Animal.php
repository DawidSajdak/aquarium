<?php

namespace Aquarium\Wildlife;

use Aquarium\Wildlife\Animal\StaminaLevel;

/**
 * Interface Animal
 * @package Aquarium\Wildlife
 */
abstract class Animal
{
    /**
     * @var string
     */
    protected $staminaLevel;

    /**
     * Animal constructor.
     * @param $staminaLevel
     */
    public function __construct($staminaLevel)
    {
        $this->staminaLevel = $staminaLevel;
    }

    public function eat()
    {
        if ($this->staminaLevel === StaminaLevel::LOW) {
            $this->staminaLevel = StaminaLevel::HIGH;
        }
    }

    public function swim()
    {
        if ($this->staminaLevel === StaminaLevel::HIGH) {
            $this->staminaLevel = StaminaLevel::LOW;
        }
    }

    /**
     * @return string
     */
    public function isHungry()
    {
        return $this->staminaLevel;
    }

    /**
     * @return string
     */
    abstract public function getSwimmingStyle();

    /**
     * @return array
     */
    abstract public function getFavoriteFoodTypes();
}
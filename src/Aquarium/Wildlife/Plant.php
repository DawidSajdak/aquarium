<?php

namespace Aquarium\Wildlife;

/**
 * Interface Plant
 * @package Aquarium\Wildlife
 */
interface Plant
{
    /**
     * @return string
     */
    public function plantName();

    /**
     * @return string
     */
    public function plantColor();
}
<?php

namespace Aquarium\Wildlife\Plant;

use Aquarium\Wildlife\Plant;

/**
 * Class LudwigiaPalustris
 * @package Aquarium\Wildlife\Plant
 */
final class LudwigiaPalustris implements Plant
{
    /**
     * @return string
     */
    public function plantName()
    {
        return 'Ludwigia palustris';
    }

    /**
     * @return string
     */
    public function plantColor()
    {
        return 'Red';
    }
}
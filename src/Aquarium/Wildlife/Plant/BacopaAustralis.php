<?php

namespace Aquarium\Wildlife\Plant;

use Aquarium\Wildlife\Plant;

/**
 * Class BacopaAustralis
 * @package Aquarium\Wildlife
 */
final class BacopaAustralis implements Plant
{
    /**
     * @return string
     */
    public function plantName()
    {
        return 'Bacopa australis';
    }

    /**
     * @return string
     */
    public function plantColor()
    {
        return 'Light green';
    }
}
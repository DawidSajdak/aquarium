<?php

namespace Aquarium\Wildlife\Animal;

use Aquarium\Wildlife\Animal;

/**
 * Class Fish
 * @package Aquarium\Wildlife\Animal
 */
final class Fish extends Animal
{
    /**
     * @return string
     */
    public function getSwimmingStyle()
    {
        return SwimmingStyle::BACKSTROKE;
    }

    /**
     * @return array
     */
    public function getFavoriteFoodTypes()
    {
        return [
            FoodTypes::APPLE,
            FoodTypes::BANANA
        ];
    }
}
<?php

namespace Aquarium\Wildlife\Animal;

use Aquarium\Wildlife\Animal;

/**
 * Class Turtle
 * @package Aquarium\Wildlife\Animal
 */
final class Turtle extends Animal
{
    /**
     * @return string
     */
    public function getSwimmingStyle()
    {
        return SwimmingStyle::CRAWL_STROKE;
    }

    /**
     * @return array
     */
    public function getFavoriteFoodTypes()
    {
        return [
            FoodTypes::ORANGE,
            FoodTypes::TOMATO
        ];
    }
}
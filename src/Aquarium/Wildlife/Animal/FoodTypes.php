<?php

namespace Aquarium\Wildlife\Animal;

/**
 * Interface FoodTypes
 * @package Aquarium\Wildlife\Animal
 */
interface FoodTypes
{
    const TOMATO = 'tomato';
    const APPLE = 'apple';
    const ORANGE = 'orange';
    const BANANA = 'banana';
}
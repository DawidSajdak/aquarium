<?php

namespace Aquarium\Wildlife\Animal;

/**
 * Interface SwimmingStyle
 * @package Aquarium\Wildlife\Animal
 */
interface SwimmingStyle
{
    const BACKSTROKE = 'backstroke';
    const CRAWL_STROKE = 'Crawl stroke';
}
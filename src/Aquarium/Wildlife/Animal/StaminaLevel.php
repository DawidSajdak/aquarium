<?php

namespace Aquarium\Wildlife\Animal;

/**
 * Interface StaminaLevel
 * @package Aquarium\Wildlife\Animal
 */
interface StaminaLevel
{
    const HIGH = 'high';
    const LOW = 'low';
}
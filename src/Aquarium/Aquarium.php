<?php

namespace Aquarium;

use Aquarium\Provider\MailProvider;
use Aquarium\Provider\SmsProvider;
use Aquarium\Wildlife\Animal;
use SharedKernel\Email;
use SharedKernel\MobilePhone;

/**
 * Class Aquarium
 * @package Aquarium
 */
final class Aquarium
{
    /**
     * @var Wildlife[]
     */
    private $wildLives;

    /**
     * @var MailProvider
     */
    private $mailProvider;

    /**
     * @var SmsProvider
     */
    private $smsProvider;

    /**
     * @param MailProvider $mailProvider
     * @param SmsProvider $smsProvider
     * @param Heater $heater
     */
    public function __construct(MailProvider $mailProvider, SmsProvider $smsProvider, Heater $heater)
    {
        $this->wildLives = [];
        $this->mailProvider = $mailProvider;
        $this->smsProvider = $smsProvider;
    }

    /**
     * @param Wildlife $wildlife
     */
    public function addWildLife(Wildlife $wildlife)
    {
        $this->wildLives[] = $wildlife;

        $this->mailProvider->addRecipient(new Email('twoja.stara@buziaczek.pl'));
        $this->mailProvider->send();

        $this->smsProvider->addRecipient(new MobilePhone('700600500'));
        $this->smsProvider->send();
    }

    public function turnOnTheLights()
    {
        foreach ($this->wildLives as $wildlife) {
            if ($wildlife instanceof Animal) {
                $wildlife->swim();
            }
        }
    }

    /**
     * @param $foodType
     */
    public function feedAnimals($foodType)
    {
        foreach ($this->wildLives as $wildlife) {
            if ($wildlife instanceof Animal && $wildlife->isHungry() && in_array($foodType, $wildlife->getFavoriteFoodTypes())) {
                $wildlife->eat();
            }
        }
    }
}